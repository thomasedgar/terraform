#
#  This is how you did it prior to version 0.9 - but since deprecated and not
#  going to work for after that.  It was a crappy way anyhow because it is not
#  in a terraform state file - i.e., 'out-of-band'.
#
#  At this time (June 03, 2018), I'm using version 1.21 which 
#  this web page discusses the latest approach.
#
#  See: http://2ndwatch.com/blog/migratingterraformremoate/
#  
#
terraform remote config \
	-backend=s3 \
	-backend-config="bucket=terraform-up-and-running-state-thomasbedgar" \
	-backend-config="key=global/s3/terraform.tfstate" \
	-backend-config="region=us-east-1" \
	-backend-config="encrypt=true"
