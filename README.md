
# Terraform up and running examples - Our instantiation

When I begain reading the book * [Terraform Up & Running](https://www.terraformupandrunning.com/), I thought I would share and allow others to contribute (for now with pull requests) to get the book examples working in AWS (and with other providers) - and eventually with containers.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.
### Prerequisites

You will need an AWS account among other things we will note here

### Prerequisites

[get an aws free account](https://aws.amazon.com/free/)
install aws cli (on unbuntu apt install awscli)
[install terraform from](https://www.terraform.io/downloads.html)


### Installing

Install terraform and aws CLI as described above 


## Running the tests

TBD

## Built With

* Brute force
* Lot's of copy paste from [Terraform Up & Running](https://www.terraformupandrunning.com/)
* Contributors like you

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

Ideally we should use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Thomas Edgar ** - *Initial work* 

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to [Yevgeniy Brikman](https://github.com/brikis98)
* Inspired by how terraform is taking off and the possibility to make our lives better

